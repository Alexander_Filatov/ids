import { Injectable } from '@angular/core';
import { Subject } from "rxjs/Subject";

@Injectable()
export class EventsExchangeService {

  showPopUpToast: Subject<any> = new Subject<any>();
  noOverflow: Subject<any> = new Subject<any>();

  constructor() {}
}
