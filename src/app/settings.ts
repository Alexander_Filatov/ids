import { environment } from '../environments/environment';

const pricing = {
  basic: 999,
  plus: 2999,
  enterprise: 3999
};

const apiRestUrlPrefix: string = environment.production ?  `` : 'http://myids.staging.image-data.com';

const apiUrls: any = {
  logInUrl: (): string => `${apiRestUrlPrefix}/myidslogin`,
  payment: (): string => `${apiRestUrlPrefix}/myidspayment`,
  registration: (): string => `${apiRestUrlPrefix}/myidsregister`,
  getCountries: (): string => `${apiRestUrlPrefix}/myidscountries`,
  resendEmail: (): string => `${apiRestUrlPrefix}/myidsresendemail`,
  isEmailExist: (): string => `${apiRestUrlPrefix}/myidscheckuser`,
  isTeamExist: (): string => `${apiRestUrlPrefix}/myidscheckteam`,
  updatePassword: (): string => `${apiRestUrlPrefix}/myidsnewpassword`
};

export {
  pricing,
  apiUrls
};
