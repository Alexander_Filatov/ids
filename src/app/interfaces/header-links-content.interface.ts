interface ContactFormInterface {
  'first-name': string;
  'last-name': string;
  'email': string;
  'company': string;
  'message': string;
  'recaptchaResolved': string;
}

export {
  ContactFormInterface
}
