interface AmountsInterface {
  basic: number;
  plus: number;
  enterprise: number;
}

interface LoginSuccessResponseInterface {
  url: string;
}

interface LoginErrorResponseBodyInterface {
  Error: string;
  ErrorCode?: string;
  expiry?: string;
  period?: string;
  plan?: string;
  team?: string;
  userid?: string;
  vendor?: string;
}

interface LoginErrorResponseInterface {
  error: LoginErrorResponseBodyInterface;
}

interface PaymentFormInterface {
  payment: string;
  plan: string;
  amount: number | string;
  period: string;
}

interface PaymentRequestBodyInterface {
  userId: number;
  teamId:  number;
  vendor: string;
  plan: string;
  period: string;
  payment_details: any;
  subscription_expiry: string;
}

interface LoginRequestBodyInterface {
  name: string;
  passwd: string;
}

export {
  AmountsInterface,
  LoginSuccessResponseInterface,
  LoginErrorResponseBodyInterface,
  LoginErrorResponseInterface,
  PaymentFormInterface,
  PaymentRequestBodyInterface,
  LoginRequestBodyInterface
}
