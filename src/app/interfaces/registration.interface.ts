interface StepOneFormInterface {
  'first-name': string;
  'last-name': string;
  'team': string;
  'email': string;
  'confirm-email': string;
  'phone': string;
  'company': string;
  'country': string;
  'city': string;
  'address': string;
  'postcode': string;
  'password': string;
}

interface StepTwoFormInterface {
  'choosen-plan': string;
  'team-size': string;
}

interface StepThreeFormInterface {
  payment: string;
  email: string;
  plan: string;
  period: string;
  amount: string | number;
  payment_details?: string;
  subscription_expiry?: string;
}

interface RegistrationSuccessBodyInterface {
  name: string;
  email: string;
  id: string;
}

interface StepThreeRequestBodyInterface {
  name: string;
  team: string;
  email: string;
  phone: string;
  company: string;
  country: string;
  city: string;
  address: string;
  postcode: string;
  password: string;
  plan: string;
  team_size: number;
  vendor: string;
  period: string;
  payment_details: string;
  subscription_expiry: string;
}

interface CountryInterface {
  display: string;
  id: number;
  name: string;
}

interface GetCountriesResponseBodyInterface {
  count: number;
  countries: CountryInterface[];
  success: true;
}

export {
  StepOneFormInterface,
  StepTwoFormInterface,
  StepThreeFormInterface,
  RegistrationSuccessBodyInterface,
  StepThreeRequestBodyInterface,
  GetCountriesResponseBodyInterface,
  CountryInterface
}
