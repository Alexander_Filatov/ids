import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { TransferDataService } from './transfer-data.service';

@Injectable()
export class CanActivateService implements CanActivate {

  constructor(private transferDataService: TransferDataService,
              private router: Router) { }

  canActivate(): boolean {
    !this.transferDataService.logInErrorBody && this.router.navigate(['../']);
    return !!this.transferDataService.logInErrorBody;
  }
}
