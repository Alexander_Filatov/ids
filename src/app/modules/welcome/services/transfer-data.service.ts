import { Injectable } from '@angular/core';
import { LoginErrorResponseBodyInterface } from '../../../interfaces';

@Injectable()
export class TransferDataService {

  logInErrorBody: LoginErrorResponseBodyInterface = null;
}
