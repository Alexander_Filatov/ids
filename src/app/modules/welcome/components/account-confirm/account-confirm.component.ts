import { Component, OnInit, EventEmitter, OnDestroy} from '@angular/core';
import { Router, ActivatedRoute, Params } from "@angular/router";
import { FormGroup, FormBuilder, Validators, ValidationErrors, AbstractControl } from '@angular/forms';
import { RequestService } from "../../../common-features/services/request.service";
import {EventsExchangeService} from '../../../../services';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import {
  LoginErrorResponseInterface
} from '../../../../interfaces';

@Component({
  selector: 'app-account-confirm',
  templateUrl: './account-confirm.component.html',
  styleUrls: ['./account-confirm.component.scss']
})
export class AccountConfirmComponent implements OnInit, OnDestroy {

  closeModal: EventEmitter<null> = new EventEmitter<null>();
  setPasswordForm: FormGroup;
  code: string = '';
  isSpinnerShow: boolean = false;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private requestService: RequestService,
              private eventsExchangeService: EventsExchangeService,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.queryParams
      .subscribe((params: Params) => {
        if (params['code']) {
          this.code = params['code'];
          this.eventsExchangeService.noOverflow.next(true);
        } else {
          this.router.navigate(['../']);
        }
      });
    this.setPasswordForm = this.formBuilder.group ({
      'password': ['', [Validators.required, Validators.minLength(8)] ],
      'confirm-password': [{value: '', disabled: true}, [Validators.required], this.isPasswordsIdentical.bind(this) ],
    });
  }

  ngOnDestroy(): void {
    this.goBack();
  }

  setPassword(): void {
    this.isSpinnerShow = true;
    let requestData: {code: string, password: string} = {
      code: this.code,
      password: this.setPasswordForm.value.password
    };
    this.requestService.updatePassword(requestData)
      .subscribe(
        () => {
          this.showNotification('Your account is activated, please login', 'success');
          this.router.navigate(['../../login'], {relativeTo: this.activatedRoute});
        },
        (resp: LoginErrorResponseInterface) => {
          this.isSpinnerShow = false;
          this.showNotification(resp.error.Error, 'error');
        })
  }

  showCurrentError(fieldName: string, validator: string): boolean {
    return this.setPasswordForm.controls[fieldName].errors && this.setPasswordForm.controls[fieldName].errors[validator];
  }

  private goBack(): void {
    this.eventsExchangeService.noOverflow.next(false);
    this.closeModal.emit();
  }

  private isPasswordsIdentical(data: AbstractControl): Observable<ValidationErrors> {
    return Observable.create((observer: Observer<{identical: string}>) => {
      if (data.value === this.setPasswordForm.value['password']) {
        observer.next(null);
      } else {
        observer.next({identical: 'not identical'});
      }
      observer.complete();
    });
  }

  private showNotification(message: string, type: string): void {
    this.eventsExchangeService.showPopUpToast.next(
      {
        message: message,
        title: '',
        type: type
      });
  }
}
