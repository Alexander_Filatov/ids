import { Component, OnInit } from '@angular/core';
import { pricing } from '../../../../settings';
import { AmountsInterface } from '../../../../interfaces';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

  pricing: AmountsInterface  = pricing;

  constructor() { }

  ngOnInit(): void {
  }
}
