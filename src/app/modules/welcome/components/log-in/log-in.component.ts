import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { EventsExchangeService } from '../../../../services';
import { RequestService } from '../../../common-features/services';
import { TransferDataService } from '../../services';
import { LoginSuccessResponseInterface, LoginErrorResponseInterface } from '../../../../interfaces';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss']
})
export class LogInComponent implements OnInit {

  logInForm: FormGroup;
  logInFailed: boolean = false;
  errorMessage: string = 'Something wrong. Error.';
  isSpinnerShow: boolean = false;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private requestService: RequestService,
              private eventsExchangeService: EventsExchangeService,
              private transferDataService : TransferDataService) { }

  ngOnInit(): void {
    this.logInForm = this.formBuilder.group ({
      'name': ['', [Validators.required] ],
      'passwd': ['', [Validators.required] ]
    });
    this.logInForm.valueChanges
      .subscribe(() => {
        this.logInFailed = false;
    });
  }

  formSubmit(): void {
    this.isSpinnerShow = true;
    this.requestService.logIn(this.logInForm.value)
      .subscribe(
        (data: LoginSuccessResponseInterface) => {
          console.log(data)
          this.isSpinnerShow = false;
          window.location.replace(window.location.origin + '/' + data.url);
      },
        (resp: LoginErrorResponseInterface) => {
          this.isSpinnerShow = false;
          if (resp.error && resp.error['Error']) {
            this.showNotification(resp.error.Error, 'error');
            if (resp.error['ErrorCode'] === '1010') {
              this.transferDataService.logInErrorBody = resp.error;
              this.router.navigate(['../payments'], {relativeTo: this.activatedRoute});
            }
            this.errorMessage = resp.error['Error'];
          }
          this.logInFailed = true;
        });
  }

  showNotification(message: string, type: string): void {
    this.eventsExchangeService.showPopUpToast.next(
      {
        message: message,
        title: '',
        type: type
      });
  }
}
