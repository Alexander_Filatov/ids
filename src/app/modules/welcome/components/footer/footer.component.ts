import { Component, OnInit } from '@angular/core';
import {Router, RouterEvent, NavigationEnd} from '@angular/router';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})

export class FooterComponent implements OnInit {

  isRegButtonVisible: boolean = true;

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.router.events
      .filter((event: RouterEvent) => event instanceof NavigationEnd)
      .map((event: RouterEvent) => event.url.indexOf('/registration') === -1)
      .subscribe((isFound: boolean) => {
        this.isRegButtonVisible = isFound;
      });
  }
}
