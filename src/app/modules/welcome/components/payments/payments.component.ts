import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { pricing } from '../../../../settings';
import { EventsExchangeService } from '../../../../services';
import { TransferDataService } from '../../services';
import { RequestService } from '../../../common-features/services';
import { AmountsInterface, PaymentFormInterface, LoginErrorResponseBodyInterface, PaymentRequestBodyInterface, IdInterface, LoginErrorResponseInterface } from '../../../../interfaces';
import * as moment from 'moment';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent implements OnInit {

  paymentForm: PaymentFormInterface;
  pricing: AmountsInterface = pricing;
  userData: LoginErrorResponseBodyInterface = null;
  isSpinnerShow: boolean = false;

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              private eventsExchangeService: EventsExchangeService,
              private requestService: RequestService,
              private transferDataService : TransferDataService) { }

  ngOnInit(): void {
    this.userData = this.transferDataService.logInErrorBody;
    this.paymentForm = {
      'payment': this.userData ? this.userData.vendor : 'stripe',
      'plan': this.userData ? this.userData.plan : 'plus',
      'period': this.userData ? this.userData.period : 'annually',
      'amount':  0
    };
    this.calculateAnnual();
  }

  planChanged(): void {
    this.paymentForm.amount = this.pricing[this.paymentForm.plan];
    this.calculateAnnual();
  }

  calculateAnnual(): void {
    this.paymentForm.amount = this.paymentForm.period === 'monthly' ? (this.pricing[this.paymentForm.plan]/1200).toFixed(2) : this.pricing[this.paymentForm.plan]/100;
  }

  paymentSuccess(payment: any): void {
    this.isSpinnerShow = true;
    let requestBody: PaymentRequestBodyInterface = {
      userId: parseInt(this.userData.userid),
      teamId: parseInt(this.userData.team),
      vendor: this.paymentForm.payment,
      plan: this.paymentForm.plan,
      period: this.paymentForm.period,
      payment_details: JSON.stringify(payment),
      subscription_expiry: this.paymentForm.period === 'monthly' ? moment().add(1, 'months').format('YYYY-MM-DD') :
        moment().add(1, 'years').format('YYYY-MM-DD')
    };

    this.requestService.payment(requestBody)
      .subscribe(
        (resp: IdInterface) => {
          console.log(resp)
          this.showNotification('Payment successful', 'success');
          this.isSpinnerShow = false;
          this.router.navigate(['../login'], {relativeTo: this.activatedRoute});
       },
      (error: LoginErrorResponseInterface) => {
        this.isSpinnerShow = false;
        this.showNotification(error.error.Error, 'error');
       });
  }

  showNotification(message: string, type: string): void {
    this.eventsExchangeService.showPopUpToast.next(
      {
        message: message,
        title: '',
        type: type
      });
  }
}
