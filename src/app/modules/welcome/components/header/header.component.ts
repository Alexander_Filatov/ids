import { Component, OnInit } from '@angular/core';
import {Router, RouterEvent, NavigationEnd} from '@angular/router';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  isLinksVisibles: boolean = false;
  isRegButtonVisible: boolean = true;

  constructor(private router: Router) { }

  ngOnInit() {
    this.router.events
      .filter((event: RouterEvent) => event instanceof NavigationEnd)
      .map((event: RouterEvent) => event.url.indexOf('/registration') === -1)
      .subscribe((isFound: boolean) => {
        this.isRegButtonVisible = isFound;
      });
  }

  toggleLinksVisibility(state: boolean): void {
    this.isLinksVisibles = state;
  }
}
