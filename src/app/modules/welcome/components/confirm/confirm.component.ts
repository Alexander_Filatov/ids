import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RequestService } from "../../../common-features/services/request.service";
import { EventsExchangeService } from '../../../../services';
import {
  LoginErrorResponseInterface
} from '../../../../interfaces';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {

  isCodeError: boolean = false;
  message: string = '';
  resendForm: FormGroup;
  resendFailed: boolean = false;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private requestService: RequestService,
              private eventsExchangeService: EventsExchangeService,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute.queryParams
      .subscribe((params: Params) => {
      if (params['code']) {
        let code: string = params['code'];
        if(code === '2003') {
          this.isCodeError = true;
          this.message = "Invalid activation request. Repeat activation email";
        }
        if(code === '2002') {
          this.message = "Your account has been activated. Please login";
        }
      } else {
        this.router.navigate(['../']);
      }
    });
    this.resendForm = this.formBuilder.group ({
      'email': ['', [Validators.required, Validators.pattern(/[A-Za-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/)]]
    });
    this.resendForm.valueChanges
      .subscribe(() => {
        this.resendFailed = false;
      });
  }

  showCurrentError(fieldName: string, validator: string): boolean {
    return this.resendForm.controls[fieldName].errors && this.resendForm.controls[fieldName].errors[validator];
  }

  formSubmit(): void {
    this.requestService.resendEmail(this.resendForm.value)
      .subscribe(
      () => {
        this.showNotification('Email sent', 'success');
        this.router.navigate(['../']);
    },
      (resp: LoginErrorResponseInterface) => {
        this.showNotification(resp.error.Error, 'error');
        this.resendFailed = true;
    })
  }

  showNotification(message: string, type: string): void {
    this.eventsExchangeService.showPopUpToast.next(
      {
        message: message,
        title: '',
        type: type
      });
  }
}
