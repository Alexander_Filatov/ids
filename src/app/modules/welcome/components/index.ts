export * from './footer/footer.component';
export * from './header/header.component';
export * from './landing/landing.component';
export * from './log-in/log-in.component';
export * from './main/main.component';
export * from './payments/payments.component';
export * from './confirm/confirm.component';
export * from './account-confirm/account-confirm.component';
