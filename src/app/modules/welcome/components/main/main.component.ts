import { Component, OnInit, HostBinding } from '@angular/core';
import { Router, NavigationEnd, RouterEvent } from '@angular/router';
import {EventsExchangeService} from '../../../../services';
import 'rxjs/add/operator/filter';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})

export class MainComponent implements OnInit {

  @HostBinding('class.no-overflow') noOverflow: boolean = false;

  constructor(private router: Router,
              private eventsExchangeService: EventsExchangeService) { }

  ngOnInit(): void {
    this.router.events
      .filter((event: RouterEvent) => event instanceof NavigationEnd)
      .subscribe(() => {
        window.scrollTo(0, 0);
      });

    this.eventsExchangeService.noOverflow
      .subscribe((flag: boolean) => {
      setTimeout(() => {
        this.noOverflow = flag;
      }, 0)
    })
  }
}
