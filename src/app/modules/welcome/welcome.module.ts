import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ScrollToModule } from 'ng2-scroll-to';
import { CommonFeaturesModule } from '../common-features';
import { WelcomeRoutingModule } from './welcome-routing.module';
import {
  MainComponent,
  HeaderComponent,
  FooterComponent,
  LandingComponent,
  LogInComponent,
  PaymentsComponent,
  ConfirmComponent,
  AccountConfirmComponent} from './components';
import { TransferDataService, CanActivateService } from './services';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    WelcomeRoutingModule,
    ScrollToModule.forRoot(),
    CommonFeaturesModule
  ],
  declarations: [
    MainComponent,
    HeaderComponent,
    FooterComponent,
    LandingComponent,
    LogInComponent,
    PaymentsComponent,
    ConfirmComponent,
    AccountConfirmComponent
  ],
  providers: [
    TransferDataService,
    CanActivateService
  ]
})
export class WelcomeModule { }
