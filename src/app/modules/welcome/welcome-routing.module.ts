import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CanActivateService } from './services';
import {
  MainComponent,
  LandingComponent,
  LogInComponent,
  PaymentsComponent,
  ConfirmComponent,
  AccountConfirmComponent} from './components';

const routes: Routes = [
  {
    path: 'main',
    component: MainComponent,
    children: [
      {
        path: 'landing',
        component: LandingComponent,
        children: [
          {
            path: 'account',
            component: AccountConfirmComponent
          },
        ]
      },
      {
        path: 'login',
        component: LogInComponent
      },
      {
        path: 'payments',
        component: PaymentsComponent,
        canActivate: [CanActivateService]
      },
      {
        path: 'confirm',
        component: ConfirmComponent
      },
      {
        path: 'registration',
        loadChildren: '../registration/registration.module#RegistrationModule'
      },
      {
        path: 'information',
        loadChildren: '../header-links-content/header-links-content.module#HeaderLinksContentModule'
      }
    ]
  },
  { path: '', redirectTo: 'main/landing', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WelcomeRoutingModule { }
