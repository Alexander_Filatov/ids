import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
  RegistrationComponent,
  StepOneComponent,
  StepTwoComponent,
  StepThreeComponent,
  SuccessComponent } from './components';
import { CanActivateService } from './services';

const routes: Routes = [
  { path: '',
    component: RegistrationComponent,
    canActivateChild: [
      CanActivateService
    ],
    children: [
      { path: 'step-one', component: StepOneComponent },
      { path: 'step-two', component: StepTwoComponent },
      { path: 'step-three', component: StepThreeComponent },
      { path: 'success', component: SuccessComponent },
      { path: '**', redirectTo: 'step-one' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegistrationRoutingModule { }
