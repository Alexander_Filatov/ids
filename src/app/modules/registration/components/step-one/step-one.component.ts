import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd, RouterEvent } from '@angular/router';
import { FormGroup, FormBuilder, Validators, ValidationErrors, AbstractControl } from '@angular/forms';
import { ShareDataService } from '../../services';
import { RequestService } from '../../../common-features';
import {EventsExchangeService} from '../../../../services';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import {
  StepOneFormInterface,
  GetCountriesResponseBodyInterface,
  LoginErrorResponseInterface,
  CountryInterface } from '../../../../interfaces';

@Component({
  selector: 'app-step-one',
  templateUrl: './step-one.component.html',
  styleUrls: ['./step-one.component.scss']
})
export class StepOneComponent implements OnInit {

  stepOneForm: FormGroup;
  mask: any[] = ['+', /[0-9]/, /[0-9]/, ' ', '(', /[0-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d{0,1}/];
  countries: CountryInterface[] = [];
  phoneCode: string = '+44 (___) ___-____';
  isUsedTimeout: any = null;
  isEmailExist: boolean = false;
  isTeamExist: boolean = false;
  checkInProcess: {[key: string]: boolean} = {
    isTeamExist: false,
    isEmailExist: false
  };

  constructor(private formBuilder: FormBuilder,
              private shareDataService: ShareDataService,
              private requestService: RequestService,
              private eventsExchangeService: EventsExchangeService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    const savedFormObject: StepOneFormInterface = this.shareDataService.stepOneFormObject;
    this.stepOneForm = this.formBuilder.group ({
      'first-name': [savedFormObject ? savedFormObject['first-name'] : '',
        [Validators.required, Validators.pattern(/^[a-zA-Z]+$/)] ],
      'last-name': [savedFormObject ? savedFormObject['last-name'] : '',
        [Validators.required, Validators.pattern(/^[a-zA-Z]+$/)] ],
      'team': [savedFormObject ? savedFormObject['team'] : '',
        [Validators.required, Validators.pattern(/^[a-zA-Z0-9\- _]+$/), Validators.maxLength(15)], this.isTeamExists.bind(this) ],
      'email': [savedFormObject ? savedFormObject['email'] : '',
        [Validators.required, Validators.pattern(/[A-Za-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/)], this.isEmailExists.bind(this) ],
      'confirm-email': [{value: savedFormObject ? savedFormObject['confirm-email'] : '', disabled: true},
        [Validators.required], this.isEmailsIdentical.bind(this) ],
      'phone': [savedFormObject ? savedFormObject['phone'] : this.phoneCode,
        [this.phonePattern.bind(this)] ],
      'company': [savedFormObject ? savedFormObject['company'] : ''  ],
      'country': [savedFormObject ? savedFormObject['country'] : 'UNITEDKINGDOM' ],
      'city': [savedFormObject ? savedFormObject['city'] : '', [Validators.required ]],
      'address': [savedFormObject ? savedFormObject['address'] : '', [Validators.required ] ],
      'postcode': [savedFormObject ? savedFormObject['postcode'] : '', [Validators.required ] ],
      'password': [savedFormObject ? savedFormObject['password'] : '',
        [Validators.required, Validators.minLength(8)] ],
      'confirm-password': [{value: savedFormObject ? savedFormObject['confirm-password'] : '', disabled: true},
        [Validators.required], this.isPasswordsIdentical.bind(this) ],
    });
    this.router.events
      .filter((event: RouterEvent) => event instanceof NavigationEnd)
      .map((event: RouterEvent) => (event.url.indexOf('registration/step-two') !== -1 || event.url.indexOf('registration/step-three') !== -1))
      .map((match: boolean) => match && this.shareDataService.stepOneFormObject && this.stepOneForm.valid)
      .subscribe((match: boolean) => {
        if (match) {this.shareDataService.stepOneFormObject = this.stepOneForm.value;}
      });
    this.fillCountries();
  }

  fillCountries(): void {
    this.requestService.getCountries()
      .subscribe(
        (data: GetCountriesResponseBodyInterface) => {
          this.countries = data.countries;
    },
      (resp: LoginErrorResponseInterface) => {
        this.showNotification(resp.error.Error, 'error');
    })
  }

  formSubmit(): void {
    this.shareDataService.stepOneFormObject = this.stepOneForm.value;
    this.router.navigate(['../step-two'], {relativeTo: this.activatedRoute, preserveQueryParams: true});
  }

  showCurrentError(fieldName: string, validator: string): boolean {
    return this.stepOneForm.controls[fieldName].errors && this.stepOneForm.controls[fieldName].errors[validator];
  }

  contryChanged(): void {
    // after codes will be added to countries array or after api to get codes will be created, variable code will get it value from there.
    let code: string[] = ''.replace(/\D/g, '').split('');
    this.phoneCode = `${code.join('')}`;
    let currentValue: string[] = this.stepOneForm.value['phone'].length ? this.stepOneForm.value['phone'].split('') : '+__ (___) ___-____'.split('');
    let newString: string = currentValue.reduce((previousValue, currentValue) => {
      if ((!isNaN(parseInt(currentValue)) || currentValue === '_') && code.length) {
        return previousValue + code.shift();
      }
      return previousValue + currentValue
    }, '');
    this.stepOneForm.patchValue({
      'phone': newString
    });
  }

  phonePattern(data: AbstractControl): {pattern: string} {
    if (data.value.replace(/\D/g, '') === this.phoneCode.replace(/\D/g, '') || !data.value.length) {
      return null;
    } else {
      if (/\+\d{2} \(\d{3}\) \d{3}-\d{4}\B/.test(data.value)) {
        return null;
      } else {
        return {pattern: 'pattern'};
      }
    }
  }

  isEmailExists(data: AbstractControl): Observable<ValidationErrors> {
    return this.isUsed({email: data.value}, 'isEmailExist')
  }

  isTeamExists(data: AbstractControl): Observable<ValidationErrors> {
    return this.isUsed({team: data.value}, 'isTeamExist')
  }

  isUsed(requestBody, requestMethodName): Observable<ValidationErrors> {
    this.isUsedTimeout && clearTimeout(this.isUsedTimeout);
      return Observable.create((observer: Observer<{exists: boolean}>) => {
        this.isUsedTimeout = setTimeout(() => {
          this.checkInProcess[requestMethodName] = true;
          this.requestService[requestMethodName](requestBody)
            .subscribe(
              (resp: {exists: true}) => {
                if (resp['exists']) {
                  observer.next(resp);
                } else {
                  observer.next(null);
                }
                this[requestMethodName] = resp['exists'];
                this.checkInProcess[requestMethodName] = false;
                observer.complete();
              },
              (resp: LoginErrorResponseInterface) => {
                this.showNotification(resp.error.Error, 'error');
                this.checkInProcess[requestMethodName] = false;
              });
          clearTimeout(this.isUsedTimeout);
        }, 1000);
      })
  }

  isEmailsIdentical(data: AbstractControl): Observable<ValidationErrors> {
    return Observable.create((observer: Observer<{identical: string}>) => {
      if (data.value === this.stepOneForm.value['email']) {
        observer.next(null);
      } else {
        observer.next({identical: 'not identical'});
      }
        observer.complete();
    });
  }

  isPasswordsIdentical(data: AbstractControl): Observable<ValidationErrors> {
    return Observable.create((observer: Observer<{identical: string}>) => {
      if (data.value === this.stepOneForm.value['password']) {
        observer.next(null);
      } else {
        observer.next({identical: 'not identical'});
      }
      observer.complete();
    });
  }

  showNotification(message: string, type: string): void {
    this.eventsExchangeService.showPopUpToast.next(
      {
        message: message,
        title: '',
        type: type
      });
  }
}
