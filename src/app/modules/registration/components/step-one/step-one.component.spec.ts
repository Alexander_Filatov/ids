import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {
  RouterTestingModule
} from '@angular/router/testing';
import { TextMaskModule } from 'angular2-text-mask';
import { ShareDataService } from '../../services/share-data.service';
import { StepOneComponent } from './step-one.component';

const mockShareDataService = {
  stepOneFormObject: null,
  stepTwoFormObject: null
};

describe('StepOneComponent', () => {
  let component: StepOneComponent;
  let fixture: ComponentFixture<StepOneComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      schemas: [
        NO_ERRORS_SCHEMA
      ],
      imports: [
        ReactiveFormsModule,
        FormsModule,
        TextMaskModule,
        RouterTestingModule
      ],
      declarations: [
        StepOneComponent
      ],
      providers: [
        { provide: ShareDataService, useValue: mockShareDataService },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StepOneComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form invalid when empty', () => {
    expect(component.stepOneForm.valid).toBeFalsy();
  });

  it('first-name field validity', () => {
    const firstName = component.stepOneForm.controls['first-name'];
    expect(firstName.valid).toBeFalsy();

    let errors = firstName.errors || {};
    expect(errors['required']).toBeTruthy();

    firstName.setValue('123');
    errors = firstName.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['pattern']).toBeTruthy();

    firstName.setValue('test');
    errors = firstName.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['pattern']).toBeFalsy();
  });

  it('last-name field validity', () => {
    const lastName = component.stepOneForm.controls['last-name'];
    expect(lastName.valid).toBeFalsy();

    let errors = lastName.errors || {};
    expect(errors['required']).toBeTruthy();

    lastName.setValue('123');
    errors = lastName.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['pattern']).toBeTruthy();

    lastName.setValue('test');
    errors = lastName.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['pattern']).toBeFalsy();
  });

  it('team-name field validity', () => {
    const teamName = component.stepOneForm.controls['team-name'];
    expect(teamName.valid).toBeFalsy();

    let errors = teamName.errors || {};
    expect(errors['required']).toBeTruthy();

    teamName.setValue('@#$test');
    errors = teamName.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['pattern']).toBeTruthy();

    teamName.setValue('test1 -_');
    errors = teamName.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['pattern']).toBeFalsy();
  });

  it('email field validity', () => {
    const email = component.stepOneForm.controls['email'];
    expect(email.valid).toBeFalsy();

    let errors = email.errors || {};
    expect(errors['required']).toBeTruthy();

    email.setValue('test');
    errors = email.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['pattern']).toBeTruthy();

    email.setValue('test@test.com');
    errors = email.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['pattern']).toBeFalsy();
  });

  it('confirm-email field validity', () => {
    const email = component.stepOneForm.controls['email'];
    const confirmEmail = component.stepOneForm.controls['confirm-email'];
    expect(confirmEmail.valid).toBeFalsy();
    expect(confirmEmail.disabled).toBe(true);
    confirmEmail.enable();

    let errors = confirmEmail.errors || {};
    expect(errors['required']).toBeTruthy();

    email.setValue('test@test.com');
    confirmEmail.setValue('not-test@test.com');
    errors = confirmEmail.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['identical']).toBeTruthy();

    confirmEmail.setValue('test@test.com');
    errors = confirmEmail.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['identical']).toBeFalsy();
  });

  it('phone field validity', () => {
    const phone = component.stepOneForm.controls['phone'];
    expect(phone.valid).toBeTruthy();

    phone.setValue('123124');
    let errors = phone.errors || {};
    expect(errors['pattern']).toBeTruthy();

    phone.setValue(`+12 (123) 123-4567`);
    errors = phone.errors || {};
    expect(errors['pattern']).toBeFalsy();
  });

  it('company field validity', () => {
    const company = component.stepOneForm.controls['company'];
    expect(company).toBeTruthy();
    expect(company.valid).toBeTruthy();
  });

  it('country field validity', () => {
    const country = component.stepOneForm.controls['country'];
    expect(country.valid).toBeTruthy();
    expect(country.value).toBe('uk');
  });

  it('password field validity', () => {
    const password = component.stepOneForm.controls['password'];
    expect(password.valid).toBeFalsy();

    let errors = password.errors || {};
    expect(errors['required']).toBeTruthy();

    password.setValue('test');
    errors = password.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['minlength']).toBeTruthy();

    password.setValue('testtest');
    errors = password.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['minlength']).toBeFalsy();
  });

  it('confirm-password field validity', () => {
    const password = component.stepOneForm.controls['password'];
    const confirmPassword = component.stepOneForm.controls['confirm-password'];
    expect(confirmPassword.valid).toBeFalsy();
    expect(confirmPassword.disabled).toBe(true);
    confirmPassword.enable();

    let errors = confirmPassword.errors || {};
    expect(errors['required']).toBeTruthy();

    password.setValue('12345678');
    confirmPassword.setValue('87654321');
    errors = confirmPassword.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['identical']).toBeTruthy();

    confirmPassword.setValue('12345678');
    errors = confirmPassword.errors || {};
    expect(errors['required']).toBeFalsy();
    expect(errors['identical']).toBeFalsy();
  });
});
