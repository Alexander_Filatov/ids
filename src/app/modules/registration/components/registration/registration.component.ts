import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, RouterEvent, NavigationEnd } from '@angular/router';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  isStepsShow: boolean = true;

  constructor(private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.router.events
      .filter((event: RouterEvent) => event instanceof NavigationEnd)
      .map((event: RouterEvent) => event.url.indexOf('/registration/success') === -1)
      .subscribe((event: boolean) => {
        this.isStepsShow = event;
      });
  }

}
