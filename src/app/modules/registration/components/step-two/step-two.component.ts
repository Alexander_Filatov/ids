import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {Router, ActivatedRoute, NavigationEnd, RouterEvent} from '@angular/router';
import {FormGroup, FormBuilder, Validators, ValidationErrors} from '@angular/forms';
import {ShareDataService} from '../../services/share-data.service';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import { pricing } from '../../../../settings';
import { AmountsInterface } from '../../../../interfaces';

@Component({
  selector: 'app-step-two',
  templateUrl: './step-two.component.html',
  styleUrls: ['./step-two.component.scss']
})
export class StepTwoComponent implements OnInit {

  pricing: AmountsInterface  = pricing;
  stepTwoForm: FormGroup;
  teamSize: number = 0;
  @ViewChild('timeSize') timeSizeRef: ElementRef;
  fieldsDisabled: boolean = false;

  constructor(private formBuilder: FormBuilder,
              private shareDataService: ShareDataService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    const plan: string = this.activatedRoute.queryParams['value'].hasOwnProperty('plan') ?
      this.activatedRoute.queryParams['value'].plan : 'plus';
    this.teamSizeDefine(plan);
    const savedFormObject: any = this.shareDataService.stepTwoFormObject;
    this.stepTwoForm = this.formBuilder.group({
      'choosen-plan': [savedFormObject ? savedFormObject['choosen-plan'] : plan],
      'team-size': [savedFormObject ? savedFormObject['team-size'] : this.teamSize]
    });
    Observable.fromEvent(this.timeSizeRef.nativeElement, 'input')
      .debounceTime(100)
      .subscribe((event: Event) => {
        this.teamSizeChanged(event);
      });
    this.router.events
      .filter((event: RouterEvent) => event instanceof NavigationEnd)
      .map((event: RouterEvent) => (event.url.indexOf('registration/step-one') !== -1 || event.url.indexOf('registration/step-three') !== -1))
      .subscribe((match: boolean) => {
        if (match && this.shareDataService.stepTwoFormObject && this.stepTwoForm.valid && !this.shareDataService.stepThreeFormObject) {
          this.shareDataService.stepTwoFormObject = this.stepTwoForm.value;
        }
      });
    this.fieldsDisabled = !!this.shareDataService.stepThreeFormObject;
  }

  teamSizeDefine(plan: string): void {
    if (plan === 'basic') {
      this.teamSize = 5;
    }
    if (plan === 'plus') {
      this.teamSize = 20;
    }
    if (plan === 'enterprise') {
      this.teamSize = 1000;
    }
  }

  planChanged(plan: string): void {
    if (plan === 'basic' && this.teamSize > 5) {
      this.teamSize = 5;
    }
    if (plan === 'plus' && this.teamSize > 20) {
      this.teamSize = 20;
    }
    this.stepTwoForm.patchValue({
      'team-size': this.teamSize
    });
  }

  teamSizeChanged(event: Event): void {
    const currentValue: number = Number.parseInt(event.target['value']);
    let plan: string = this.stepTwoForm.value['choosen-plan'];
    if (currentValue > 1000) {
      event.target['value'] = 1000;
    }
    if (currentValue <= 0) {
      event.target['value'] = 1;
    }
    if (currentValue > this.stepTwoForm.value['team-size']) {
      if (currentValue > 5) {
        plan = 'plus';
      }
      if (currentValue > 20) {
        plan = 'enterprise';
      }
    }
    this.stepTwoForm.patchValue({
      'choosen-plan': plan,
      'team-size': event.target['value']
    });
  }

  formSubmit(): void {
    if (!this.shareDataService.stepThreeFormObject) {
      this.shareDataService.stepTwoFormObject = this.stepTwoForm.value;
    }
    this.router.navigate(['../step-three'], {relativeTo: this.activatedRoute});
  }
}
