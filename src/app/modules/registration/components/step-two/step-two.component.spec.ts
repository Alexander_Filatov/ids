import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {
  RouterTestingModule
} from '@angular/router/testing';
import { StepTwoComponent } from './step-two.component';
import { ShareDataService } from '../../services/share-data.service';

const mockShareDataService = {
  stepOneFormObject: null,
  stepTwoFormObject: null
};

describe('StepTwoComponent', () => {
  let component: StepTwoComponent;
  let fixture: ComponentFixture<StepTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, FormsModule, RouterTestingModule],
      declarations: [ StepTwoComponent ],
        providers: [
          { provide: ShareDataService, useValue: mockShareDataService },
        ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
