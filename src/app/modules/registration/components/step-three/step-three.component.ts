import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {ShareDataService} from '../../services/share-data.service';
import {RequestService} from '../../../common-features/services';
import {EventsExchangeService} from '../../../../services';
import {pricing} from '../../../../settings';
import {
  AmountsInterface,
  StepOneFormInterface,
  StepTwoFormInterface,
  StepThreeFormInterface,
  LoginErrorResponseInterface,
  RegistrationSuccessBodyInterface,
  StepThreeRequestBodyInterface
} from '../../../../interfaces';
import * as moment from 'moment';

@Component({
  selector: 'app-step-three',
  templateUrl: './step-three.component.html',
  styleUrls: ['./step-three.component.scss']
})
export class StepThreeComponent implements OnInit {

  stepThreeForm: StepThreeFormInterface;
  pricing: AmountsInterface = pricing;
  isSpinnerShow: boolean = false;
  errorMessage: string = '';
  requestBody: StepThreeRequestBodyInterface = null;
  tryAgainHidden: boolean = true;
  fieldsDisabled: boolean = false;

  constructor(private shareDataService: ShareDataService,
              private requestService: RequestService,
              private eventsExchangeService: EventsExchangeService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    const stepOneFormObject: StepOneFormInterface = this.shareDataService.stepOneFormObject;
    const stepTwoFormObject: StepTwoFormInterface = this.shareDataService.stepTwoFormObject;
    const savedFormObject: StepThreeFormInterface = this.shareDataService.stepThreeFormObject;
    this.stepThreeForm = {
      'payment': savedFormObject ? savedFormObject.payment : 'stripe',
      'email': stepOneFormObject ? stepOneFormObject['email'] : '',
      'plan': stepTwoFormObject ? stepTwoFormObject['choosen-plan'] : 'plus',
      'period': savedFormObject ? savedFormObject.period : 'annually',
      'amount': savedFormObject ? savedFormObject.amount : 0,
      'payment_details': savedFormObject ? savedFormObject.payment_details : '',
      'subscription_expiry': savedFormObject ? savedFormObject.subscription_expiry : ''
    };
    this.tryAgainHidden = !savedFormObject;
    this.fieldsDisabled = !!savedFormObject;
  }

  calculateAnnual(): void {
    this.stepThreeForm.amount = this.stepThreeForm.period === 'monthly' ?
      (this.pricing[this.stepThreeForm.plan] / 1200).toFixed(2) :
      this.pricing[this.stepThreeForm.plan] / 100;
  }

  sendRegistrationData(): void {
    this.requestBody = {
      name: this.shareDataService.stepOneFormObject['first-name'] + " " + this.shareDataService.stepOneFormObject['last-name'],
      team: this.shareDataService.stepOneFormObject.team,
      email: this.shareDataService.stepOneFormObject.email,
      phone: this.shareDataService.stepOneFormObject.phone,
      company: this.shareDataService.stepOneFormObject.company,
      country: this.shareDataService.stepOneFormObject.country,
      city: this.shareDataService.stepOneFormObject.city,
      address: this.shareDataService.stepOneFormObject.address,
      postcode: this.shareDataService.stepOneFormObject.postcode.toString(),
      password: this.shareDataService.stepOneFormObject.password,
      team_size: parseInt(this.shareDataService.stepTwoFormObject['team-size']),
      vendor: this.stepThreeForm.payment,
      plan: this.stepThreeForm.plan,
      period: this.stepThreeForm.period,
      payment_details: this.stepThreeForm.payment_details,
      subscription_expiry: this.stepThreeForm.subscription_expiry
    };
    this.isSpinnerShow = true;
    this.requestService.registration(this.requestBody)
      .subscribe(
        (data: RegistrationSuccessBodyInterface) => {
          console.log(data)
          this.isSpinnerShow = false;
          this.shareDataService.stepOneFormObject = null;
          this.shareDataService.stepTwoFormObject = null;
          this.shareDataService.stepThreeFormObject = null;
          this.router.navigate(['../success'], {relativeTo: this.activatedRoute});
        },
        (resp: LoginErrorResponseInterface) => {
          this.isSpinnerShow = false;
          this.tryAgainHidden = false;
          if (resp.error && resp.error['Error']) {
            console.log(resp.error)
            this.showNotification(resp.error.Error, 'error');
            this.errorMessage = resp.error['Error'];
          }
        });
  }

  paymentSuccess(payment: any): void {
    this.stepThreeForm.payment_details = JSON.stringify(payment);
    this.stepThreeForm.subscription_expiry = this.stepThreeForm.period === 'monthly' ?
      moment().add(1, 'months').format('YYYY-MM-DD') :
      moment().add(1, 'years').format('YYYY-MM-DD');
    this.shareDataService.stepThreeFormObject = this.stepThreeForm;
    this.fieldsDisabled = true;
    this.showNotification('Payment was successful', 'success');
    this.sendRegistrationData();
  }

  showNotification(message: string, type: string): void {
    this.eventsExchangeService.showPopUpToast.next(
      {
        message: message,
        title: '',
        type: type
      });
  }
}
