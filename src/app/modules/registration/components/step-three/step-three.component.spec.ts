import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {
  RouterTestingModule
} from '@angular/router/testing';
import { StepThreeComponent } from './step-three.component';
import { StripeDirective } from '../../directives/stripe.directive';
import { PayPalDirective } from '../../directives/pay-pal.directive';
import { ShareDataService } from '../../services/share-data.service';

const mockShareDataService = {
  stepOneFormObject: null,
  stepTwoFormObject: null
};

describe('StepThreeComponent', () => {
  let component: StepThreeComponent;
  let fixture: ComponentFixture<StepThreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepThreeComponent, StripeDirective, PayPalDirective ],
      imports: [ReactiveFormsModule, FormsModule, RouterTestingModule],
        providers: [
          { provide: ShareDataService, useValue: mockShareDataService },
        ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
