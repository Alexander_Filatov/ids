import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';
import { CommonFeaturesModule } from '../common-features';
import { RegistrationRoutingModule } from './registration-routing.module';
import {
  RegistrationComponent,
  StepOneComponent,
  StepTwoComponent,
  StepThreeComponent,
  SuccessComponent } from './components';
import {
  ShareDataService,
  CanActivateService} from './services';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RegistrationRoutingModule,
    TextMaskModule,
    CommonFeaturesModule
  ],
  declarations: [
    RegistrationComponent,
    StepOneComponent,
    StepTwoComponent,
    StepThreeComponent,
    SuccessComponent
  ],
  providers: [
    ShareDataService,
    CanActivateService
  ]
})
export class RegistrationModule { }
