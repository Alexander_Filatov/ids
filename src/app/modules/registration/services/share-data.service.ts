import { Injectable } from '@angular/core';
import { StepOneFormInterface, StepTwoFormInterface, StepThreeFormInterface, StepThreeRequestBodyInterface } from '../../../interfaces';

@Injectable()
export class ShareDataService {

  stepOneFormObject: StepOneFormInterface = null;
  stepTwoFormObject: StepTwoFormInterface = null;
  stepThreeFormObject: StepThreeFormInterface = null;
  requestBody: StepThreeRequestBodyInterface = null;
}
