import { TestBed, inject } from '@angular/core/testing';
import {
  RouterTestingModule
} from '@angular/router/testing';
import { CanActivateService } from './can-activate.service';
import { ShareDataService } from './share-data.service';

const mockShareDataService = {
  stepOneFormObject: null,
  stepTwoFormObject: null
};

describe('CanActivateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CanActivateService,
        { provide: ShareDataService, useValue: mockShareDataService }
      ],
      imports: [RouterTestingModule]
    });
  });

  it('should be created', inject([CanActivateService], (service: CanActivateService) => {
    expect(service).toBeTruthy();
  }));
});
