import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivateChild,
  Router } from '@angular/router';
import { ShareDataService } from './share-data.service';

@Injectable()
export class CanActivateService implements CanActivateChild {

  constructor(private shareDataService: ShareDataService) { }

  canActivateChild(route: ActivatedRouteSnapshot): boolean {
    if (route.routeConfig.path === 'step-one' || route.routeConfig.path === 'success') {
      return true;
    }
    if (route.routeConfig.path === 'step-two') {
      return !!this.shareDataService.stepOneFormObject;
    }
    if (route.routeConfig.path === 'step-three') {
      return !!this.shareDataService.stepOneFormObject && !!this.shareDataService.stepTwoFormObject;
    }
    return false;
  }
}
