import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RecaptchaModule } from 'ng-recaptcha';

import { HeaderLinksContentRoutingModule } from './header-links-content-routing.module';
import {
  AboutComponent,
  ContactComponent,
  TermsComponent } from './components';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    HeaderLinksContentRoutingModule,
    RecaptchaModule.forRoot()
  ],
  declarations: [
    AboutComponent,
    ContactComponent,
    TermsComponent
  ]
})
export class HeaderLinksContentModule { }
