import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ContactFormInterface } from '../../../../interfaces';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  contactForm: FormGroup;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.contactForm = this.formBuilder.group ({
      'first-name': ['', [Validators.required, Validators.pattern(/^[a-zA-Z]+$/)] ],
      'last-name': ['', [Validators.required, Validators.pattern(/^[a-zA-Z]+$/)] ],
      'email': ['', [Validators.required, Validators.pattern(/[A-Za-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/)] ],
      'company': ['' ],
      'message': ['', [Validators.required] ],
      'recaptchaResolved': [null, [Validators.required] ],
    });
  }

  showCurrentError(fieldName: string, validator: string): boolean {
    return this.contactForm.controls[fieldName].errors && this.contactForm.controls[fieldName].errors[validator];
  }

  formSubmit(): void {
    console.log(this.contactForm);
  }

  resolved(): void {
    this.contactForm.patchValue({
      'recaptchaResolved': true
    });
  }
}
