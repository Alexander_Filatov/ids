import {Directive, ElementRef, OnInit, Renderer2, Input, Output, EventEmitter} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/fromEvent';

@Directive({
  selector: '[appStripe]'
})
export class StripeDirective implements OnInit {

  @Input() settings: any;
  @Output() paymentSuccess: EventEmitter<any> = new EventEmitter<any>();
  stripeConstructor: any;

  constructor(private renderer2: Renderer2, private elementRef: ElementRef) { }

  ngOnInit(): void {
    Observable.fromEvent(this.elementRef.nativeElement, 'click')
      .subscribe(() => {
        this.stripeConstructor.open({
          name: 'MyIDS',
          description: 'Payment details',
          email: this.settings.data['email'],
          amount: this.settings.data['amount'] * 100
        });
      });

    const self = this;
    const script: HTMLScriptElement = this.renderer2.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://checkout.stripe.com/checkout.js';
    script.addEventListener('load', () => {
      self.stripeConstructor = (<any>window).StripeCheckout.configure({
        key: 'pk_test_0Sd04Nm4sW7XKENn9JzGMfBl',
        locale: 'auto',
        token: function (token: any) {
          self.paymentSuccess.emit(token);
        }
      });
      window.addEventListener('popstate', function () {
        self.stripeConstructor.close();
      });
    });
    this.renderer2.appendChild(this.elementRef.nativeElement, script);
  }
}
