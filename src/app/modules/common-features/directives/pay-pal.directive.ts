import {Directive, ElementRef, OnInit, Renderer2, Input, Output, EventEmitter} from '@angular/core';

@Directive({
  selector: '[appPayPal]'
})
export class PayPalDirective implements OnInit {

  @Input() settings: any;
  @Output() paymentSuccess: EventEmitter<any> = new EventEmitter<any>();

  constructor(private renderer2: Renderer2, private elementRef: ElementRef) { }

  ngOnInit(): void {
    const self = this;
    const script: HTMLScriptElement = this.renderer2.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://www.paypalobjects.com/api/checkout.js';
    script.addEventListener('load', () => {
      (<any>window)['paypal'].Button.render({
        env: 'sandbox', // sandbox | production
        style: {
          label: 'paypal',
          size: 'responsive',    // small | medium | large | responsive
          shape: 'rect',     // pill | rect
          color: 'blue',     // gold | blue | silver | black
          tagline: false
        },
        // PayPal Client IDs - replace with your own
        // Create a PayPal app: https://developer.paypal.com/developer/applications/create
        client: {
          sandbox: 'AZDxjDScFpQtjWTOUtWKbyN_bDt4OgqaF4eYXlewfBP4-8aqX3PiV8e1GWU6liB2CUXlkA59kJXE7M6R',
          production: '<insert production client id>'
        },
        commit: true,
        payment: function (data, actions) {
          return actions.payment.create({
            payment: {
              transactions: [
                {
                  amount: {
                    total: self.settings.data['amount'],
                    currency: 'USD'
                  }
                }
              ]
            }
          });
        },
        onAuthorize: function (data, actions) {
          console.log(data)
          return actions.payment.execute().then(function (callbackData) {
            self.paymentSuccess.emit(callbackData);
            console.log(callbackData)
          });
        }
      }, this.elementRef.nativeElement);
    });
    this.renderer2.appendChild(this.elementRef.nativeElement, script);
  }
}
