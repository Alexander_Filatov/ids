import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from "rxjs/Observable";

import { apiUrls as config } from '../../../settings';
import { LoginSuccessResponseInterface, IdInterface, LoginRequestBodyInterface, PaymentRequestBodyInterface, StepThreeRequestBodyInterface, RegistrationSuccessBodyInterface, GetCountriesResponseBodyInterface } from '../../../interfaces';

@Injectable()
export class RequestService {

  requestOptions: {headers} = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private httpClient: HttpClient) { }

  logIn(bodyData: LoginRequestBodyInterface): Observable<LoginSuccessResponseInterface> {
    return this.httpClient.post<LoginSuccessResponseInterface>(config.logInUrl(), JSON.stringify(bodyData), this.requestOptions);
  }

  payment(bodyData: PaymentRequestBodyInterface): Observable<IdInterface> {
    return this.httpClient.post<IdInterface>(config.payment(), JSON.stringify(bodyData), this.requestOptions);
  }

  registration(bodyData: StepThreeRequestBodyInterface): Observable<RegistrationSuccessBodyInterface> {
    return this.httpClient.post<RegistrationSuccessBodyInterface>(config.registration(), JSON.stringify(bodyData), this.requestOptions);
  }

  getCountries(): Observable<GetCountriesResponseBodyInterface> {
    return this.httpClient.get<GetCountriesResponseBodyInterface>(config.getCountries(), this.requestOptions);
  }

  resendEmail(bodyData: {email: string}): Observable<{success: true}> {
    return this.httpClient.post<{success: true}>(config.resendEmail(), JSON.stringify(bodyData), this.requestOptions);
  }

  isEmailExist(bodyData: {email: string}): Observable<{exists: true}> {
    return this.httpClient.post<{exists: true}>(config.isEmailExist(), JSON.stringify(bodyData), this.requestOptions);
  }

  isTeamExist(bodyData: {team: string}): Observable<{exists: true}> {
    return this.httpClient.post<{exists: true}>(config.isTeamExist(), JSON.stringify(bodyData), this.requestOptions);
  }

  updatePassword(bodyData: {code: string, password: string}): Observable<null> {
    return this.httpClient.post<null>(config.updatePassword(), JSON.stringify(bodyData), this.requestOptions);
  }
}
