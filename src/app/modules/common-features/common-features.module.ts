import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { ModalWrapperComponent } from './components';
import { RequestService } from './services'
import {
  StripeDirective,
  PayPalDirective,
  DisableControlDirective} from './directives';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  declarations: [
    StripeDirective,
    PayPalDirective,
    DisableControlDirective,
    ModalWrapperComponent
  ],
  providers: [
    RequestService
  ],
  exports: [
    StripeDirective,
    PayPalDirective,
    DisableControlDirective,
    ModalWrapperComponent
  ]
})

export class CommonFeaturesModule {}
