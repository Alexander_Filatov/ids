import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { CommonFeaturesModule } from './modules/common-features';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EventsExchangeService } from './services';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    CommonFeaturesModule,
    ToastrModule.forRoot()
  ],
  providers: [
    EventsExchangeService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
