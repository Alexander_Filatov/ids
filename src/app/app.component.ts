import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { EventsExchangeService } from './services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private toastrService: ToastrService,
              private eventsExchangeService: EventsExchangeService) {}

  ngOnInit(): void {
    this.eventsExchangeService.showPopUpToast
      .subscribe((data) => {
        this.toastrService[data.type](
          data.title,
          data.message,
          {
            timeOut: 7000,
            closeButton: true,
            tapToDismiss: false,
            enableHtml: true
          });
      })
  }
}
